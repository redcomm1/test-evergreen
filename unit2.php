<?php

$arr = array(3,6,7,8,9,15,17,43,53,71,82,83, 85, 99, 101, 105, 106, 107);

$med = getMediana($arr);
echo 'mediana: ' . $med .  '</br>';
    for($i = 0; $i<count($arr); $i++){
        if($med >= $arr[$i] && $med <= $arr[$i+1]){
            $first = $i;
            $sec = $i+1;
            break;
        }
    }

if(count($arr)%2 == 0){
    print_r(getNearestNumbers($first , $sec, $arr )) ;
}
else{
    print_r(getNearestNumbers($first , $sec+1, $arr )) ;
}

//Функция получения медианы из массива
function getMediana($arr) {
    sort ($arr);
    $count = count($arr);
    $middle = floor($count/2);
    if ($count%2){
        return $arr[$middle];
    }
    else {
        return ($arr[$middle-1]+$arr[$middle])/2;
    }
}

//Функция, определяющая, является ли число простым
function isPrime($n){
    if($n==1)
        return false;
    for($d=2; $d*$d<=$n; $d++){
        if($n%$d==0)
            return false;
    }
    return true;
}

//Функция определения ближайших простых чисел от медианы
function getNearestNumbers($firstNum, $secNum, $arr){
    for ($i = $firstNum; $i >= 0; $i--) {
        if (isPrime($arr[$i])) {
            $firstPrime = $arr[$i];
            break;
        }
    }
    for ($i = $secNum; $i < count($arr); $i++) {
        if (isPrime($arr[$i])) {
            $secondPrime = $arr[$i];
            break;
        }
    }
    return $primeNum = array(
        'firstPrime'    => $firstPrime,
        'secondPrime'   => $secondPrime
    );
}
